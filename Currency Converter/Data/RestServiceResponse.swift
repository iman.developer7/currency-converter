//
//  RestServiceResponse.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import Foundation

typealias ResponseCompletion = (RestServiceResponse?, Error?) -> ()

struct RestServiceResponse: Decodable {
    
    var base: String?
    var rates: [String: Double]?
    var date: String?
    
}
