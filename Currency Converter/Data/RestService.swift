//
//  RestService.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import Foundation

class RestService {
    
    static let baseUrl = "https://api.exchangeratesapi.io/latest"
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    static let shared = RestService()
    
    private init() {}
    
    func fetchExchangeRate(base: String, target: String, completion: @escaping ResponseCompletion) {
        guard var urlComponents = URLComponents(string: RestService.baseUrl) else { return }
        urlComponents.query = "&base=\(base)&symbols=\(target)"
        guard let url = urlComponents.url else {return}
        let request = URLRequest(url: url)
        
        let task = defaultSession.dataTask(with: request) { (data, response, error) in
            var userResponse: RestServiceResponse?
            
            if let error = error {
                debugPrint(error.localizedDescription)
            } else if let data = data {
                let decoder = JSONDecoder()
                do {
                    userResponse = try decoder.decode(RestServiceResponse.self, from: data)
                } catch {
                    debugPrint(error.localizedDescription)
                }
            }
            
            DispatchQueue.main.async {
                completion(userResponse, error)
            }
        }
        
        task.resume()
    }
    
}





