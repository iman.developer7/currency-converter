//
//  UITextFieldExtensions.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import UIKit

extension UITextField {
    
    func canAddText(_ string: String) -> Bool {
        let newCharacter = CharacterSet(charactersIn: string)
        let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacter)
        
        guard let oldText = text else { return false }
        
        if boolIsNumber == true {
            return true
        } else {
            if string == "." {
                let countdots = oldText.components(separatedBy: ".").count - 1
                if countdots == 0 {
                    return oldText.count > 0
                } else {
                    if countdots == 1 {
                        return false
                    } else {
                        return true
                    }
                }
            } else {
                return false
            }
        }
    }
}
