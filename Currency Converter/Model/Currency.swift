//
//  Currency.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import Foundation

struct Currency {
    
    var code: String
    var symbol: String
    var name: String
    
}

class CurrencyHelpers {
    
    lazy var currenciesList: [Currency] = {
        var temp = [Currency]()
        
        temp.append(Currency(code: "EUR", symbol: "€", name:"Euro"))
        temp.append(Currency(code: "AUD", symbol: "$", name: "Australian Dollar"))
        temp.append(Currency(code: "BGN", symbol: "лв", name:"Bulgarian Lev"))
        temp.append(Currency(code: "BRL", symbol: "R$", name:"Brazilian Real"))
        temp.append(Currency(code: "CAD", symbol: "$", name:"Canadian Dollar"))
        temp.append(Currency(code: "CHF", symbol: "CHF", name:"Swiss Franc"))
        temp.append(Currency(code: "CNY", symbol: "¥", name:"Chinese Yuan Renminbi"))
        temp.append(Currency(code: "CZK", symbol: "Kč", name:"Czech Koruna"))
        temp.append(Currency(code: "DKK", symbol: "kr", name:"Danish Krone"))
        temp.append(Currency(code: "GBP", symbol: "£", name:"British Pound"))
        temp.append(Currency(code: "HKD", symbol: "HK$", name:"Hong Kong Dollar"))
        temp.append(Currency(code: "HRK", symbol: "kn", name:"Croatian Kuna"))
        temp.append(Currency(code: "HUF", symbol: "Ft", name:"Hungarian Forint"))
        temp.append(Currency(code: "IDR", symbol: "Rp", name:"Indonesian Rupiah"))
        temp.append(Currency(code: "ILS", symbol: "₪", name:"Israeli Shekel"))
        temp.append(Currency(code: "INR", symbol: "₹", name:"Indian Rupee"))
        temp.append(Currency(code: "JPY", symbol: "¥", name:"Japanese Yen"))
        temp.append(Currency(code: "KRW", symbol: "₩", name:"South Korean Won"))
        temp.append(Currency(code: "MXN", symbol: "$", name:"Mexican Peso"))
        temp.append(Currency(code: "MYR", symbol: "RM", name:"Malaysian Ringgit"))
        temp.append(Currency(code: "NOK", symbol: "kr", name:"Norwegian Krone"))
        temp.append(Currency(code: "NZD", symbol: "$", name:"New Zealand Dollar"))
        temp.append(Currency(code: "PHP", symbol: "₱", name:"Philippine Peso"))
        temp.append(Currency(code: "PLN", symbol: "zł", name:"Polish Zloty"))
        temp.append(Currency(code: "RON", symbol: "lei", name:"Romanian Leu"))
        temp.append(Currency(code: "RUB", symbol: "₽", name:"Russian Ruble"))
        temp.append(Currency(code: "SEK", symbol: "kr", name:"Swedish Krona"))
        temp.append(Currency(code: "SGD", symbol: "$", name:"Singapore Dollar"))
        temp.append(Currency(code: "THB", symbol: "฿", name:"Thai Baht"))
        temp.append(Currency(code: "TRY", symbol: "t", name:"Turkish Lira"))
        temp.append(Currency(code: "USD", symbol: "$", name:"US Dollar"))
        temp.append(Currency(code: "ZAR", symbol: "R", name:"South African Rand"))
        
        return temp
    }()
    
}
