//
//  ViewController.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var baseTextField: UITextField!
    @IBOutlet weak var baseSymbolLabel: UILabel!
    @IBOutlet weak var baseCodeLabel: UILabel!
    @IBOutlet weak var baseImageView: UIImageView!
    
    @IBOutlet weak var resultTextField: UITextField!
    @IBOutlet weak var resultSymbolLabel: UILabel!
    @IBOutlet weak var resultCodeLabel: UILabel!
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var resultActivityIndicator: UIActivityIndicatorView!
    
    var currentRate = 0.0
    
    var restService: RestService?
    
    var selectingBaseCurrency = true
    
    var baseCurrency: Currency! {
        didSet {
            baseSymbolLabel.text = baseCurrency.symbol
            baseCodeLabel.text = baseCurrency.code
            baseImageView.image = UIImage(named: baseCurrency.code)
        }
    }
    
    var targetCurrency: Currency! {
        didSet {
            resultSymbolLabel.text = targetCurrency.symbol
            resultCodeLabel.text = targetCurrency.code
            resultImageView.image = UIImage(named: targetCurrency.code)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restService = RestService.shared
        
        let currencies = CurrencyHelpers.init().currenciesList
        
        baseCurrency = currencies.first(where: { $0.code == "EUR" })
        targetCurrency = currencies.first(where: { $0.code == "USD" })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateRate()
    }

    @IBAction func keyboardButtonPressed(_ sender: UIButton) {
        if sender.tag == 99 {
            baseTextField.text = ""
            resultTextField.text = ""
        } else {
            guard let text = sender.titleLabel?.text else { return }
            
            if baseTextField.canAddText(text) {
                baseTextField.text?.append(text)
            }
            updateResult()
        }
    }
    
    @IBAction func swipeCurrenciesButtonTouchUpInside(_ sender: UIButton) {
        let oldBaseCurrency = baseCurrency
        baseCurrency = targetCurrency
        targetCurrency = oldBaseCurrency
        
        updateRate()
    }
    
    @IBAction func baseCurrencyControlTouchUpInside() {
        selectingBaseCurrency = true
        showSelectCurrency(selected: baseCurrency)
    }
    
    @IBAction func resultCurrencyControlTouchUpInside() {
        selectingBaseCurrency = false
        
        showSelectCurrency(selected: targetCurrency)
    }
    
    private func updateRate() {
        resultTextField.isHidden = true
        resultActivityIndicator.startAnimating()
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Messages.errorNetworkingTitle, message: Messages.errorNetworkingMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        if baseCurrency.code == targetCurrency.code {
            self.currentRate = 1
            self.updateResult()
            self.resultActivityIndicator.stopAnimating()
            self.resultTextField.isHidden = false
            
            return
        }
        
        restService?.fetchExchangeRate(base: baseCurrency.code, target: targetCurrency.code, completion: { (response, error) in
            guard let data = response else { return }
            guard let rates = data.rates else { return }
            guard let rete = rates[self.targetCurrency.code] else { return }
            self.currentRate = rete
            self.updateResult()
            self.resultActivityIndicator.stopAnimating()
            self.resultTextField.isHidden = false
        })
        
        
    }
    
    private func updateResult() {
        guard let baseText = self.baseTextField.text else { return }
        guard let baseDouble = Double(baseText) else { return }
        self.resultTextField.text = String(baseDouble * self.currentRate)
    }
    
    private func showSelectCurrency(selected currency: Currency) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let selectCurrencyVC = storyboard.instantiateViewController(withIdentifier: "SelectCurrencyViewController") as! SelectCurrencyViewController
        selectCurrencyVC.delegate = self
        
        present(selectCurrencyVC, animated: true, completion: {selectCurrencyVC.selectedCurrency = currency})
        
    }
    
}

extension ViewController: SelectCurrencyDelegate {
    func selectCurrency(_ currency: Currency) {
        if selectingBaseCurrency {
            baseCurrency = currency
        } else {
            targetCurrency = currency
        }
        
        updateRate()
    }
}
