//
//  CurrencyTableViewCell.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var currencyImageView: UIImageView!
    @IBOutlet weak var currencyNameLabel: UILabel!
    
    @IBOutlet weak var checkMark: UIImageView!
    
    var currency: Currency! {
        didSet {
            currencyImageView.image = UIImage(named: currency.code)
            currencyNameLabel.text = "\(currency.code) - \(currency.name)"
        }
    }

}
