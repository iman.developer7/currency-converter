//
//  SelectCurrencyViewController.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import UIKit

protocol SelectCurrencyDelegate {
    
    func selectCurrency(_ currency: Currency)
    
}

class SelectCurrencyViewController: UIViewController {
    
    var selectedCurrency: Currency? {
        didSet {
            currenciesTableView.reloadData()
        }
    }
    
    var delegate: SelectCurrencyDelegate?
    
    lazy var currencies = CurrencyHelpers.init().currenciesList
    
    @IBOutlet weak var currenciesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currenciesTableView.tableFooterView = UIView()
    }
    
    
    @IBAction func cancelSelectionButtonTouchUpInside(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func doneButtonTouchUpInside(_ sender: UIButton) {
        guard let currency = self.selectedCurrency else { return }
        dismiss(animated: true) {
            self.delegate?.selectCurrency(currency)
        }
    }
    
}

extension SelectCurrencyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyTableViewCell", for: indexPath) as! CurrencyTableViewCell
        
        let currency = currencies[indexPath.row]
        
        if let selectedCurrency = self.selectedCurrency {
            cell.checkMark.isHidden = (currency.code != selectedCurrency.code)
        }
        cell.currency = currency
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCurrency = currencies[indexPath.row]
    }
    
    
}
