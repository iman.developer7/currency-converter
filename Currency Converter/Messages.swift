//
//  Messages.swift
//  Currency Converter
//
//  Created by Iman Zabihi on 08/12/2020.
//

import Foundation

struct Messages {
    
    static let errorNetworkingTitle = "No internet connection"
    static let errorNetworkingMessage = "Turn on mobile data or use Wi-Fi"
    
}

